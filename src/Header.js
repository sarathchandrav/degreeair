import React from 'react';
import { Link } from 'react-router-dom';
import './css/Header.scss'
const Header = () => {
  return (
    <div className="topNavBar" fixed='top'>
      <div>
        <a href="/" >
          DIGREE%AIR
        </a>
        <label className="lable"><Link className="link" to="/" >Home</Link></label>
        <label className="lable"><Link className="link" to="/WeatherReport">Weather Report</Link></label>
        <label className="lable"><Link className="link" to="/AirQuality" > Air Quality</Link></label>
      </div>
    </div>
  );
}
export default Header;